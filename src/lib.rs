pub use Output::Success as Success;
pub use Output::Notice as Notice;
pub use Output::Error as Error;

#[must_use]
pub enum Output<T, N, E> {
	Success(T),
	Notice(T, N),
	Error(E)
}

impl<T, N, E> Output<T, N, E> {
	pub fn is_success(&self) -> bool {
		match self {
			&Output::Success(_) => true,
			&Output::Notice(_, _) => false,
			&Output::Error(_) => false
		}
	} pub fn is_notice(&self) -> bool {
		match self {
			&Output::Success(_) => false,
			&Output::Notice(_, _) => true,
			&Output::Error(_) => false
		}
	} pub fn is_error(&self) -> bool {
		match self {
			&Output::Success(_) => false,
			&Output::Notice(_, _) => false,
			&Output::Error(_) => true
		}
	} pub fn success(self) -> Option<T> {
		match self {
			Output::Success(t) => Some(t),
			Output::Notice(_, _) => None,
			Output::Error(_) => None
		}
	} pub fn notice(self) -> Option<(T, N)> {
		match self {
			Output::Success(_) => None,
			Output::Notice(t, n) => Some((t, n)),
			Output::Error(_) => None
		}
	} pub fn error(self) -> Option<E> {
		match self {
			Output::Success(_) => None,
			Output::Notice(_, _) => None,
			Output::Error(e) => Some(e)
		}
	} pub fn unwrap(self) -> T {
		match self {
			Output::Success(t) => t,
			Output::Notice(t, _) => t,
			Output::Error(_) => panic!("")
		}
	} pub fn expect(self, msg: &str) -> T {
		match self {
			Output::Success(t) => t,
			Output::Notice(t, _) => t,
			Output::Error(_) => panic!("{}", msg)
		}
	} pub fn is_some(&self) -> bool {
		match self {
			&Output::Success(_) => true,
			&Output::Notice(_, _) => true,
			&Output::Error(_) => false
		}
	} pub fn some(self) -> Option<(T, Option<N>)> {
		match self {
			Output::Success(t) => Some((t, None)),
			Output::Notice(t, n) => Some((t, Some(n))),
			Output::Error(_) => None
		}
	}
}
